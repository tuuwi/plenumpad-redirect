from datetime import datetime, timedelta
from flask import Flask, redirect

app = Flask(__name__)


@app.route("/")
def hello_world():
    now = datetime.now()
    tuesday = now - timedelta(days=now.weekday()) + timedelta(days=1)
    day = str(tuesday.day)
    if tuesday.day < 10:
        day = "0" + str(tuesday.day)
    month = str(tuesday.month)
    if tuesday.month < 10:
        month = "0" + str(tuesday.month)
    return redirect(
        "https://pad.tuuwi.de/p/" + str(tuesday.year) + month + day + "_tuuwi_plenum",
        code=302,
    )
